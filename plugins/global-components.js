import Vue from 'vue'

import LoadingIcon from '~/components/common/loading-icon'
Vue.component('LoadingIcon', LoadingIcon)

import CardFruitInfo from '~/components/card-fruit-info.vue'
Vue.component('CardFruitInfo', CardFruitInfo)
